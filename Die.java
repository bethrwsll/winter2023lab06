import java.util.Random;
public class Die {
	private int faceValue;
	private Random random;
	
	// Constructor
	public Die() {
		this.faceValue = 1;
		random = new Random();
	}
	
	// Get Method
	public int getFaceValue() {
		return this.faceValue;
	}
	
	// Action method
	public int roll(){
		this.faceValue = random.nextInt(6) + 1;
		return this.faceValue;
	}
	
	// toString
	public String toString() {
		return "" + this.faceValue;
	}
}