public class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	// Constructor
	public Board() {
		die1 = new Die();
		die2 = new Die();
		tiles = new boolean[12];
	}
	
	//toString
	public String toString(){
		String board = "";
		for(int i = 0; i<tiles.length; i++){
			if(tiles[i]){
				board += "X ";
			}
			else {
				board += (i+1) + " ";
			}
		}
		return board;
			
	}
	
	// Action
	public boolean playATurn() {
		die1.roll();
		die2.roll();
		System.out.println("Die 1: " + die1 + ", Die 2: " + die2);
		
		int sumOfDice = die1.getFaceValue() + die1.getFaceValue();
		
		if(tiles[sumOfDice-1]){
			
			if(tiles[die1.getFaceValue()]==false) {
				tiles[die1.getFaceValue()] = true;
				System.out.println("Closing tile with the same value as die one: " + die1.getFaceValue());
				return false;
			}
			
			else if(tiles[die2.getFaceValue()-1]==false) {
				
				tiles[die2.getFaceValue()-1] = true;
				System.out.println("Closing tile with the same value as die one: " + die2.getFaceValue());
				return false;
			}
			
			else {
				
				System.out.println("All the tiles for these values are already shut");
				return true;
			}
			
		}
		
		else {
			
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + (sumOfDice-1));
		}
		
		return false;
	}
}