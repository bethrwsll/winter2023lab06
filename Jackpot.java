public class Jackpot {
	public static void main(String[] args){
		System.out.println("Welcome to the game Jackpot!");
		
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(gameOver == false) {
			
			System.out.println();
			
			System.out.println(board);
			
			if(board.playATurn()){
				gameOver = true;
			}
			else {
				numOfTilesClosed += 1;
			}
		}
		System.out.println();
		
		if(numOfTilesClosed >= 7){
			System.out.println("Player has won & reached the jackpot!");
		}
		else {
			System.out.println("Player has lost & couldn't reach the jackpot");
		}
	}
}